import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.serialization.StringDeserializer;
import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class ConsumerApp {
    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:58529");
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "group-");
        properties.put(ConsumerConfig.GROUP_INSTANCE_ID_CONFIG, "consumer-id");
        properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");

        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());

        try (Consumer<String, String> consumer = new KafkaConsumer<>(properties)) {
            consumer.subscribe(Collections.singletonList("topic_my_topic"));

            while (true) {
                System.out.println("Polling for messages...");
                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(500));
                System.out.println("Received " + records.count() + " messages");
                records.forEach(record -> System.out.println("Received message: " + record.value()));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
