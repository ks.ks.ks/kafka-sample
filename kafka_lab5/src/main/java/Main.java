
public class Main {
    public static void main(String[] args) {

        Thread producerThread = new Thread(() -> ProducerApp.main(new String[]{})); 
        producerThread.start();
        
        Thread consumerThread = new Thread(() -> ConsumerApp.main(new String[]{}));
        consumerThread.start();
    }
}


