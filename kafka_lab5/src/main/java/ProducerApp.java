import org.apache.kafka.clients.producer.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class ProducerApp {
    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:58529");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer");

        try (Producer<String, String> producer = new KafkaProducer<>(properties)) {
            for (int i = 0; i < 1000; i++) {
                String messageContent = "Message " + i + " - " + getCurrentDateTime();
                producer.send(new ProducerRecord<>("topic_my_topic", messageContent), (metadata, exception) -> {
                    if (exception == null) {
                        System.out.println("Sent message: " + metadata.offset());
                    } else {
                        exception.printStackTrace();
                    }
                });
                //Thread.sleep(1000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getCurrentDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormat.format(new Date());
    }
}
